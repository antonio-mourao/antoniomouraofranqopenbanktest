from rest_framework import serializers
from ..models import Vehicle, Garage

class VehicleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vehicle
        fields = '__all__'

class GarageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Garage
        fields = ['id', 'person', 'vehicles']
        read_only = ['id', 'person']

    vehicles = serializers.PrimaryKeyRelatedField(queryset=Vehicle.objects.all(),
                                                                    many=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        vehicles = list()

        for vehicle in representation['vehicles']:

            vehicles.append(
                VehicleSerializer(Vehicle.objects.get(
                    id=vehicle
                )).data
            )

        representation.update({
            'vehicles': vehicles
        })

        return representation

    def to_internal_value(self, data):
        data.update(
            self.context
        )
        return super().to_internal_value(data)
