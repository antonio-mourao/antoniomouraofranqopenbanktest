from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated
from .serializers import VehicleSerializer, GarageSerializer
from ..models import Vehicle, Garage

class VehicleViewSet(mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    viewsets.GenericViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer

class GarageViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    serializer_class = GarageSerializer

    def get_queryset(self):

        user = self.request.user
        queryset = Garage.objects.filter(person=user)
        return queryset

    def get_serializer_context(self):

        context = dict()
        context.update({
            'person': self.request.user.id
        })

        return context

class AllGarageViewSet(mixins.ListModelMixin,
                        viewsets.GenericViewSet):

    serializer_class = GarageSerializer
    queryset = Garage.objects.all()
