from django.db import models
from .options import VehicleTypeOption
from person.models import Person

# Create your models here.
class Vehicle(models.Model):

    model = models.CharField(max_length=255)
    color = models.CharField(max_length=255, null=True)
    year = models.PositiveSmallIntegerField()
    vehicle_type = models.CharField(
        max_length=255,
        choices=[(v.value, v.value) for v in VehicleTypeOption])

    def get_informations(self):

        information = dict()

        information.update(
            {
                'year': self.year
            }
        )

        if self.vehicle_type == VehicleTypeOption.MOTORCYCLE.value:
            information.update(
                {
                    'model': self.model
                }
            )
        else:
            information.update(
                {
                    'color': self.color
                }
            )

        return information

class Garage(models.Model):

    person = models.ForeignKey(Person,on_delete=models.CASCADE)
    vehicles = models.ManyToManyField(Vehicle)
