from django.conf.urls import include, url
from rest_framework import routers
from .api import viewsets as vs

router = routers.DefaultRouter()
router.register(r'vehicles', vs.VehicleViewSet, basename='vehicles')
router.register(r'my_garages', vs.GarageViewSet, basename='my_garages')
router.register(r'garages', vs.AllGarageViewSet, basename='garages')

urlpatterns = [
    url(r'^', include(router.urls)),
]
