from enum import Enum

class VehicleTypeOption(Enum):

    CAR = 'car'
    MOTORCYCLE = 'motorcycle'
