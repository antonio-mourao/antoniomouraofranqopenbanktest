from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from person.models import Person
from garage.models import Garage

class PersonAdmin(UserAdmin):

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2',
            'first_name', 'last_name', 'phone', 'email'),
        }),
    )

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)

        if not change:
            Garage.objects.create(
                person = obj
            )


admin.site.register(Person, PersonAdmin)
