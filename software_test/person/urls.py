from django.conf.urls import include, url
from rest_framework import routers
from .api import viewsets as vs

router = routers.DefaultRouter()
router.register(r'person', vs.PersonViewSet, basename='person')

urlpatterns = [
    url(r'^', include(router.urls)),
]
