from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils.translation import ugettext_lazy as _
# Create your models here.

class Person(AbstractUser):

    first_name = models.CharField(_('first name'), max_length=150)
    last_name = models.CharField(_('last name'), max_length=150)
    email = models.EmailField(_('email address'), unique=True,
        error_messages={
            'unique': _("A user with that username already exists."),
        })
    phone = models.PositiveIntegerField()

    objects = UserManager()
