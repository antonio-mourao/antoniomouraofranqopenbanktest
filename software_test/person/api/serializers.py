from django.db import transaction
from rest_framework import serializers
from person.models import Person
from garage.models import Garage

class PersonSerializer(serializers.ModelSerializer):

    class Meta:
        model = Person
        fields = ['first_name', 'last_name', 'email', 'phone', 'username',
                    'password1', 'password2']

    password1 = serializers.CharField()
    password2 = serializers.CharField()

    def validate(self, data):
        if data['password1'] != data['password2']:
            raise serializers.ValidationError("Passwords must be equals!")
        return super().validate(data)

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'first_name': instance.first_name,
            'last_name': instance.last_name,
            'phone': instance.phone,
            'email': instance.email
        }

    def create(self, validated_data):
        with transaction.atomic():
            password = validated_data['password1']
            del validated_data['password1']
            del validated_data['password2']
            instance = Person.objects.create(**validated_data)
            instance.set_password(password)
            instance.save()

            Garage.objects.create(
                person = instance
            )

            return instance
