from rest_framework import viewsets, mixins
from person.models import Person
from person.api.serializers import PersonSerializer
from garage.models import Garage

class PersonViewSet(mixins.CreateModelMixin,
                    mixins.ListModelMixin,
                    viewsets.GenericViewSet):

    serializer_class = PersonSerializer
    queryset = Person.objects.all()

    def get_queryset(self):
        queryset = Person.objects.all()

        with_vehicle = self.request.query_params.get('with_vehicle')

        if with_vehicle == 'False':
            queryset = Person.objects.exclude(
                id__in=Garage.objects.filter(
                    vehicles__isnull=False
                ).values_list('person', flat=True)
            )
        elif with_vehicle == 'True':
            queryset = Person.objects.filter(
                id__in=Garage.objects.filter(
                    vehicles__isnull=False
                ).values_list('person', flat=True)
            )
        return queryset
